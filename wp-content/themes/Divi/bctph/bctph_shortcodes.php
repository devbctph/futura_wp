<?php

if ( !defined( '_BCTPH_' ) )
	return;

add_shortcode('bctph-edit-profile', 'bctph_edit_profile');
function bctph_edit_profile($atts, $content = null) {
	if ( !is_user_logged_in() ) {
		$output = apply_filters( 'btcph_edit_profile_user_not_logged_in_message', '<p class="warning" id="btcph_edit_profile_user_not_logged_in_message">'.__( 'You must be logged in to edit your profile.', 'profile-builder' ) .'</p>' );
	} else {
		$user = wp_get_current_user();
		$user_id = $user->ID;

		if ( isset ( $_POST['id'] ) ) {
			bctph_user_register( $user_id );

			// update user email
			wp_update_user( array( 'ID' => $user_id, 'user_email' => $_POST['user_email'] ) );
		}

		$first_name = get_user_meta( $user_id, 'first_name' )[0];
		$last_name = get_user_meta( $user_id, 'last_name' )[0];
		$user_email = get_user_meta( $user_id, 'user_email' )[0];
		$email = $user->user_email;
		$gender = get_user_meta( $user_id, 'gender' )[0];
		$address = get_user_meta( $user_id, 'address' )[0];
		ob_start();
		?>
		<div id="my-account">
			<div id="my-account-inner">
				<form method="post">
					<h4>Name</h4>
					<input type="hidden" name="id" value="<?php echo $user_id ?>" />
					<div class="form-group et_pb_contact">
						<label for="first_name">First Name</label>
						<p><input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" /></p>
					</div>
					<div class="form-group et_pb_contact">
						<label for="last_name">Last Name</label>
						<p><input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" /></p>
					</div>
					<h4>Contact Info</h4>
					<div class="form-group et_pb_contact">
						<label for="user_email">E-mail</label>
						<p><input type="text" name="user_email" id="user_email" class="form-control" value="<?php echo esc_attr( wp_unslash( $email ) ); ?>" /></p>
					</div>
					<h4>Additional Info</h4>
					<div class="form-group et_pb_contact">
						<label for="gender">Gender</label>
						<p><?php echo bctph_generate_dropdown($user, ['id' => 'gender', 'name' => 'gender', 'class' => 'form-control'], gender_list(), 'gender') ?></p>
					</div>
					<div class="form-group et_pb_contact">
						<label for="address">Address</label>
						<p><textarea name="address" id="address" class="form-control" rows="5" cols="30"><?php echo esc_attr( wp_unslash( $address ) ); ?></textarea></p>
					</div>
					<div class="form-submit">
						<button type="submit" class="et_pb_button">Update</button>
					</div>
				</form>
			</div>
		</div>
		<?php
		$output = ob_get_clean();
	}

	return $output;
}

add_shortcode('bctph-change-password', 'bctph_change_password');
function bctph_change_password($atts, $content = null) {
	if ( !is_user_logged_in() ) {
		$output = apply_filters( 'btcph_change_password_user_not_logged_in_message', '<p class="warning" id="btcph_change_password_user_not_logged_in_message">'.__( 'You must be logged in to change your password.', 'profile-builder' ) .'</p>' );
	} else {
		$user = wp_get_current_user();
		$user_id = $user->ID;

		$output = '';
		// check if post
		if ( isset ( $_POST['id'] ) ) {
			// check if password match
			if ( $_POST['new_password'] !== $_POST['confirm_password'] ) {
				$output .= apply_filters( 'bctph_change_password_confirm_not_match_message', '<p class="warning" id="bctph_change_password_confirm_not_match_message">'.__( 'Confirm Password does not match.', 'profile-builder' ) .'</p>' );
			} else {
				if ( $user && wp_check_password( $_POST['current_password'], $user->data->user_pass, $user_id ) ) {
					bctph_user_change_password( $user, $_POST );
					$output .= apply_filters( 'bctph_change_password_success_message', '<p class="warning" id="bctph_change_password_success_message">'.__( 'You successfully changed your password.', 'profile-builder' ) .'</p>' );
				} else {
					$output .= apply_filters( 'bctph_change_password_not_match_message', '<p class="warning" id="bctph_change_password_not_match_message">'.__( 'Password does not match.', 'profile-builder' ) .'</p>' );
				}
			}
		}
		ob_start();
		?>
		<div id="my-account">
			<div id="my-account-inner">
				<form method="post">
					<input type="hidden" name="id" value="<?php echo $user_id ?>" />
					<div class="form-group et_pb_contact">
						<label for="current_password">Current Password</label>
						<p><input type="password" name="current_password" id="current_password" class="form-control" value="<?php echo esc_attr( wp_unslash( $password ) ); ?>" /></p>
					</div>
					<div class="form-group et_pb_contact">
						<label for="new_password">New Password</label>
						<p><input type="password" name="new_password" id="new_password" class="form-control" value="<?php echo esc_attr( wp_unslash( $password ) ); ?>" /></p>
					</div>
					<div class="form-group et_pb_contact">
						<label for="confirm_password">Confirm Password</label>
						<p><input type="password" name="confirm_password" id="confirm_password" class="form-control" value="<?php echo esc_attr( wp_unslash( $confirm_password ) ); ?>" /></p>
					</div>
					<div class="form-submit">
						<button type="submit" class="et_pb_button">Update</button>
					</div>
				</form>
			</div>
		</div>
		<?php
		$output .= ob_get_clean();
	}

	return $output;
}