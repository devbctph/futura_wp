<?php

if ( !defined( '_BCTPH_' ) )
	return;

add_action( 'wp_enqueue_scripts', 'bctph_themes_setup' );
function bctph_themes_setup() {
	wp_enqueue_style( 'bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css', '4.0.0' );

	wp_enqueue_script( 'bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/js/bootstrap.min.js', array( 'jquery' ), '4.0.0' );
}


//CUSTOMIZE ADMIN PAGE

function my_loginURL() {
    return 'http://www.futura-web.jp';
}
add_filter('login_headerurl', 'my_loginURL');


function my_loginURLtext() {
    return 'Futura';
}
add_filter('login_headertitle', 'my_loginURLtext');


function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/login/login_style.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');
