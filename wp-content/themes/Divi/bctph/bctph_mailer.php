<?php

if ( !defined( '_BCTPH_' ) )
    return;

// custom mailer
add_action( 'phpmailer_init', 'bctph_phpmailer_example' );
function bctph_phpmailer_example( $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.gmail.com';
    $phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
    $phpmailer->Port = 587;
    $phpmailer->Username = 'devbctph@gmail.com';
    $phpmailer->Password = 'Go0gle1234';

    // Additional settings…
    //$phpmailer->SMTPSecure = "tls"; // Choose SSL or TLS, if necessary for your server
    $phpmailer->From = "devbctph@gmail.com";
    $phpmailer->FromName = "Futura Team";
}