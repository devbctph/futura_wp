<?php

if ( !defined( '_BCTPH_' ) )
	return;

add_action('after_setup_theme', 'bctph_remove_admin_bar');
function bctph_remove_admin_bar() {
	if ( !current_user_can( 'administrator' ) && !is_admin() ) {
	  show_admin_bar(false);
	}
}