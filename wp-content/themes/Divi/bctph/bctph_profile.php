<?php

if ( !defined( '_BCTPH_' ) )
	return;

// form helper
function bctph_generate_dropdown($user, $params, $options, $key = '', $value = '') {
	$enable_selected = !empty($key) ? true : false;
	$user_meta_key = ($enable_selected) ? get_user_meta( $user->ID, $key )[0] : $value;
	?>
	<select name="<?php echo $params['name'] ?>" id="<?php echo $params['id'] ?>" class="<?php echo isset( $params['class'] ) ? $params['class'] : 'input'; ?>">
		<?php foreach ($options as $option) {
			printf('<option value="%s" %s>%s</option>', $option['value'], ($option['value'] == $user_meta_key ? 'selected' : ''), $option['label']);
		} ?>
	</select>
	<?php
}

function gender_list() {
	return [
		[
			'value' => 'male',
			'label' => 'Male'
		],
		[
			'value' => 'female',
			'label' => 'Female'
		]
	];
}

// additional field in registration
add_action( 'register_form', 'bctph_registration_form' );
function bctph_registration_form() {
	$first_name = ( ! empty( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
	$last_name = ( ! empty( $_POST['last_name'] ) ) ? trim( $_POST['last_name'] ) : '';
	$gender = ( ! empty( $_POST['gender'] ) ) ? trim( $_POST['gender'] ) : '';
	$address = ( ! empty( $_POST['address'] ) ) ? trim( $_POST['address'] ) : '';
	?>
	<p>
		<label for="first_name"><?php _e( 'First Name', 'futura' ) ?><br>
		<input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" size="32" />
	</p>
	<p>
		<label for="last_name"><?php _e( 'Last Name', 'futura' ) ?><br>
		<input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr( wp_unslash( $last_name ) ); ?>" size="32" />
	</p>
	<p>
		<label for="gender"><?php _e( 'Gender', 'futura' ) ?><br>
		<?php echo bctph_generate_dropdown($user, ['id' => 'gender', 'name' => 'gender'], gender_list(), '', $gender) ?>
	</p>
	<p>
		<label for="address"><?php _e( 'Address', 'futura' ) ?><br>
		<textarea name="address" id="address" class="input" rows="5" cols="30"><?php echo esc_attr( wp_unslash( $address ) ); ?></textarea>
	</p>
	<?php
}

// add form validation
add_filter( 'registration_errors', 'bcpth_registration_errors', 10, 3 );
add_action( 'user_profile_update_errors', 'bcpth_registration_errors', 10, 3 );
function bcpth_registration_errors( $errors, $sanitized_user_login, $user_email ) {
    if ( empty( $_POST['first_name'] ) || ! empty( $_POST['first_name'] ) && trim( $_POST['first_name'] ) == '' ) {
        $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: Please type your first name.', 'futura' ) );
    }
        
    if ( empty( $_POST['last_name'] ) || ! empty( $_POST['last_name'] ) && trim( $_POST['last_name'] ) == '' ) {
        $errors->add( 'last_name_error', __( '<strong>ERROR</strong>: Please type your last name.', 'futura' ) );
    }

    return $errors;
}

// save extra registration meta
add_action( 'user_register', 'bctph_user_register' );
function bctph_user_register( $user_id ) {
    if ( ! empty( $_POST['first_name'] ) ) {
        update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
    }

    if ( ! empty( $_POST['last_name'] ) ) {
        update_user_meta( $user_id, 'last_name', trim( $_POST['last_name'] ) );
    }

    if ( ! empty( $_POST['user_email'] ) ) {
        update_user_meta( $user_id, 'user_email', trim( $_POST['user_email'] ) );
    }

    if ( ! empty( $_POST['gender'] ) ) {
        update_user_meta( $user_id, 'gender', trim( $_POST['gender'] ) );
    }

    if ( ! empty( $_POST['address'] ) ) {
        update_user_meta( $user_id, 'address', trim( $_POST['address'] ) );
    }
}

// fix change password auto login issue
add_action( 'wp', 'auto_login' );
function auto_login() {
	if ( !is_user_logged_in() ) {
		$cookie_values = $_COOKIE;
		foreach ($cookie_values as $cookie_key => $cookie_value) {
			if ( strpos( $cookie_key, 'wordpress_logged_in' ) !== false ) {
				list( $user_login, $timestamp, $hash, $hash_again ) = explode('|', $cookie_value);

				$user = get_userdatabylogin( $user_login );
				$user_id = $user->ID;
		        wp_set_current_user( $user_id, $user_login );
		        wp_set_auth_cookie( $user_id );
		        do_action( 'wp_login', $user_login );
			}
		}
	}
}

function bctph_user_change_password( $user, $post_data ) {
	$user_id = $user->ID;

	$new_password = wp_slash( $post_data['new_password'] );

	// change password
	$update = wp_set_password( $new_password, $user_id );
}

// add new user in admin
add_action( 'user_new_form', 'bctph_admin_registration_form' );
function bctph_admin_registration_form( $operation ) {
	if ( 'add-new-user' !== $operation ) {
		// $operation may also be 'add-existing-user'
		return;
	}
	?>
	<h3><?php esc_html_e( 'Additional Information', 'futura' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="gender"><?php _e( 'Gender', 'futura' ) ?></label></th>
			<td>
				<?php echo bctph_generate_dropdown($user, ['id' => 'gender', 'name' => 'gender'], gender_list()) ?>
			</td>
		</tr>
		<tr>
			<th><label for="address"><?php _e( 'Address', 'futura' ) ?></label></th>
			<td>
				<textarea name="address" id="address" class="input" rows="5" cols="30"></textarea>
			</td>
		</tr>
	</table>
	<?php
}

add_action( 'show_user_profile', 'bctph_show_extra_profile_fields', 999 );
add_action( 'edit_user_profile', 'bctph_show_extra_profile_fields', 999 );

// show additional info in profile
function bctph_show_extra_profile_fields( $user ) {
	?>
	<h3><?php esc_html_e( 'Additional Information', 'futura' ); ?></h3>

	<table class="form-table">
		<tr>
			<th><label for="gender"><?php esc_html_e( 'Gender', 'futura' ); ?></label></th>
			<td>
				<?php echo bctph_generate_dropdown($user, ['id' => 'gender', 'name' => 'gender'], gender_list(), 'gender') ?>
			</td>
		</tr>
		<tr>
			<th><label for="address"><?php esc_html_e( 'Address', 'futura' ); ?></label></th>
			<td>
				<textarea name="address" id="address" class="input" rows="5" cols="30"><?php echo esc_attr( wp_unslash( get_user_meta( $user->ID, 'address' )[0] ) ); ?></textarea>
			</td>
		</tr>
	</table>
	<?php
}

add_action('edit_user_profile_update', 'bctph_update_extra_profile_fields');
function bctph_update_extra_profile_fields( $user_id ) {
	if ( ! empty( $_POST['gender'] ) ) {
        update_user_meta( $user_id, 'gender', trim( $_POST['gender'] ) );
    }

	if ( ! empty( $_POST['address'] ) ) {
        update_user_meta( $user_id, 'address', trim( $_POST['address'] ) );
    }
}