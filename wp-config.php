<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'futuraweb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4yAu`RB]f#].a4A`Ye yXZd1b+NPJ6zTYLm=j+Fz>ZFHlHFL3.IO1 JGYSK@~Thg');
define('SECURE_AUTH_KEY',  '97,n@CL^d?7^j%2py~?;(Wv`~jp.C{;wM|lR4[JAW_xVI~Ujz{]pB$L6xb2Ne{Q0');
define('LOGGED_IN_KEY',    ')kVsz.^A@gdZC{#`-c_*p7_ArxZ!eO7]am9=CvV%xl#e,;9N*)>wQ8i3_a`YN^UJ');
define('NONCE_KEY',        ';BiBNK8FLO/IT#}xbSY!;pW vGu^z_S>.NbeeOVnr*%pMDLQ1jfVjW9Cml.|$t6%');
define('AUTH_SALT',        'ruxAFP3<4&xc*U=[(+H9wzwK/qn8HxBP42PjmVr}^oob|qr>En)O(XcpLJ2J-|:{');
define('SECURE_AUTH_SALT', '${-L2l]e9-d!}c4t2Z2$3oeO<oh#uyl~<!=Us.>S(=dv+ xdf!h<+Rl{L=eLYFKY');
define('LOGGED_IN_SALT',   'RQ*pZX?vXXmVwzz[<X7Kk$ZC=8F_qL5:KidHKklz+lmv3f?~EFy}S?C8a <Vd/Vq');
define('NONCE_SALT',       '^h|[98VfsEFmEKITqZua(Mnqa=^n]]<UQ$`Ud]Ed;JD @TkD89SGvT>,&c*{,j(2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FORCE_SSL_ADMIN', false);
